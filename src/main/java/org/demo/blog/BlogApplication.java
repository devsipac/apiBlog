package org.demo.blog;

import org.demo.blog.config.CustomUserDetails;
import org.demo.blog.dao.UserDao;
import org.demo.blog.model.Role;
import org.demo.blog.model.User;
import org.demo.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@SpringBootApplication
public class BlogApplication {

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}

	/**
	 * Password grants are switched on by injecting an AuthenticationManager.
	 * Here, we setup the builder so that the userDetailsService is the one we coded.
	 * @param builder
	 * @param userDao
	 * @throws Exception
	 */
	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, UserDao userDao, UserService service) throws Exception {
		//Setup a default user if db is empty
		if (userDao.count()==0)
			service.save(new User("admin", "admin", Arrays.asList(new Role("USER"), new Role("ACTUATOR"))));
		builder.userDetailsService(userDetailsService(userDao)).passwordEncoder(passwordEncoder);
	}

	/**
	 * We return an istance of our CustomUserDetails.
	 * @param userDao
	 * @return
	 */
	private UserDetailsService userDetailsService(final UserDao userDao) {
		return username -> new CustomUserDetails(userDao.findByUsername(username));
	}
}
