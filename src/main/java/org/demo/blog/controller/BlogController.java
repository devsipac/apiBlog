package org.demo.blog.controller;

import org.demo.blog.config.CustomUserDetails;
import org.demo.blog.model.Post;
import org.demo.blog.service.PostService;
import org.demo.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class BlogController {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/posts")
    public List<Post> posts(){
        return postService.getAllPosts();
    }

    @PostMapping(value = "/post")
    public String addPost(@RequestBody Post post){
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

//        Add Date with this IF
        if (post.getDate() == null )
            post.setDate(new Date());
        post.setCreator(userService.getUser(userDetails.getUsername()));
        postService.insert(post);

        return "Post publicado";
    }

    @GetMapping(value="/posts/{username}")
    public List<Post> postsByUser(@PathVariable String username){
        return postService.findByUser(userService.getUser(username));
    }


}
