package org.demo.blog.controller;

import org.demo.blog.model.Role;
import org.demo.blog.model.User;
import org.demo.blog.pjos.UserRegistration;
import org.demo.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/register")
    public String register(@RequestBody UserRegistration userRegistration  ){
//        Password Confirm
        if (!userRegistration.getPassword().equals(userRegistration.getPasswordConfirm()))
            return "Las contraseñas no coinciden";

//        Existing user
        else if (userService.getUser(userRegistration.getUsername()) !=null)
            return "El usuario ya existe";
//        Create user with role User
        userService.save(new User(userRegistration.getUsername(),userRegistration.getPassword(), Arrays.asList(new Role("User"))));

        return "Usuario creado con exito";
    }

    @GetMapping(value = "/users")
    public List<User> users(){
        return userService.getAllUsers();
    }

    @Autowired
    private TokenStore tokenStore;

    @GetMapping(value = "/logouts")
    public void logout(@RequestParam (value = "access_token") String accessToken){
        tokenStore.removeAccessToken(tokenStore.readAccessToken(accessToken));
    }

}
