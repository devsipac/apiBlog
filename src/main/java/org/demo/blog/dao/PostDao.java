package org.demo.blog.dao;

import org.demo.blog.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostDao extends JpaRepository<Post,Long>{

    List<Post> findByCreatorId(Long id);
}
