package org.demo.blog.dao;

import org.demo.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User repository for CRUD operations.
 */
public interface UserDao extends JpaRepository<User,Long> {
    User findByUsername(String username);
}