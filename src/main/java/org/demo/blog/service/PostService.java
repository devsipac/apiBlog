package org.demo.blog.service;

import org.demo.blog.dao.PostDao;
import org.demo.blog.model.Post;
import org.demo.blog.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
    @Autowired
    private PostDao postDao;

    public List<Post> getAllPosts(){
        return postDao.findAll();
    }

    public void insert(Post post){
        postDao.save(post);
    }

    public List<Post> findByUser(User user){
        return postDao.findByCreatorId(user.getId());
    }

}
